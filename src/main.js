import { createApp } from 'vue'
import { createNotifier } from './notifications/index.js'
import App from './App.vue'
import './index.css'

import { unref } from 'vue'

const vm = createApp(App)
.use(createNotifier())
.mount('#app')

