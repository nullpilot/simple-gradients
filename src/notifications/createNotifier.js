import { ref, unref } from 'vue'
import {
  notifyKey,
  notificationsKey
} from './injectionSymbols'

export function createNotifier() {
  let id = 0
  const notifications = ref([])
  const notify = (n, timeout = 3000) => {
    n._id = id++
    notifications.value.push(n)
    setTimeout(removeNotification.bind(null, n), timeout)
  }
  const removeNotification = (n) => {
    notifications.value.splice(notifications.value.indexOf(n), 1)
  }

  return {
    install(app) {
      app.provide(notifyKey, notify)
      app.provide(notificationsKey, notifications)
    }
  }
}
