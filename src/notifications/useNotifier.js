import { inject } from 'vue';
import {
  notifyKey,
  notificationsKey
} from './injectionSymbols'

export function useNotify() {
  return inject(notifyKey)
}

export function useNotifications() {
  return inject(notificationsKey)
}
